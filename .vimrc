syntax on       " syntax highlighting
set ruler       " show the cursor position
set hlsearch    " highlight the last searched term
colorscheme desert

" tab space correction
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set autoindent
set mouse=a

" replace <ESC> with homekeys jk
inoremap jk <ESC>
inoremap kj <ESC>

" make undo and redo something I can remember
nnoremap <C-Z> u 
nnoremap <C-Y> <C-R>
inoremap <C-Z> <C-O>u
inoremap <C-Y> <C-O><C-R>

" Use ctrl-[hjkl] to select the active split
nmap <silent> <C-h> :wincmd h:call<CR>
nmap <silent> <C-j> :wincmd j:call<CR>
nmap <silent> <C-k> :wincmd k:call<CR>
nmap <silent> <C-l> :wincmd l:call<CR>

" tab change
nnoremap <C-I> gt

" find and replace all occurances of word with \r <substution>
nnoremap <leader>r yiw:%s/\<<C-r>"\>//g<left><left>

" use template files for new files with the corresponding extension
function AddTemplate(tmpl_file)
    exe "0read " . a:tmpl_file
    let substDict = {}
    let substDict["filename"] = expand('%:t')
    let substDict["year"] = strftime("%Y")
    let substDict["date"] = strftime("%Y-%m-%d")
    exe '%s/<<\([^>]*\)>>/\=substDict[submatch(1)]/g'
    set nomodified
    normal G
endfunction

if has("autocmd")
    augroup templates
        "autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
        "autocmd BufNewFile *.py 0r ~/.vim/templates/skeleton.py
        autocmd BufNewFile *.sh call AddTemplate("~/.vim/templates/skeleton.sh")
        autocmd BufNewFile *.py call AddTemplate("~/.vim/templates/skeleton.py")
    augroup END
endif
